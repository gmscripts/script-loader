/**
 * Load script from server
 */
class ScriptLoader {

	/**
	 * Create class object
	 * @param serverUrl - url for request
	 */
	constructor(serverUrl) {
		this.serverUrl = serverUrl;
	}

	/**
	 * Load script from server
	 * @param scriptDbSelector - script table selector like: name=script_name
	 * @param onLoad - function will be run after script load
	 */
	loadScript(scriptDbSelector, onLoad) {
		let scriptLoadedEvent = scriptData => {

			let isActivated = scriptData.activated && scriptData.activated !== '0';
			if (! isActivated)
				console.debug(`ScriptLoader: Don't run not activated script "${scriptDbSelector}"`);

			let urlNotMatch = ! ScriptLoader._checkPageUrlMatch(scriptData.url);
			if (urlNotMatch)
				console.debug(`ScriptLoader: Url fail for script with selector "${scriptDbSelector}"`);

			if (!urlNotMatch && isActivated && onLoad)
				onLoad(scriptData.text);
		};
		this._sendLoadScriptRequest(scriptDbSelector, scriptLoadedEvent);
	}

	/**
	 * Check page url is match
	 * @param scriptUrl - url from script table
	 * @returns {boolean} true, if current page url match with url in script
	 * @private
	 */
	static _checkPageUrlMatch(scriptUrl) {
		let thisUrl = window.location.href;
		let starCharIndex = scriptUrl.indexOf('*');

		if (starCharIndex === 0) return true;

		else if (starCharIndex > 0) {
			let thisUrlBeforeStarPosition = thisUrl.substr(0, starCharIndex);
			let scriptUrlBeforeStarPosition = scriptUrl.substr(0, starCharIndex);
			return (thisUrlBeforeStarPosition === scriptUrlBeforeStarPosition)
		}
		else {
			return thisUrl === scriptUrl
		}
	}

	/**
	 * Send get script request to server
	 * @param scriptDbSelector - selector string
	 * @param onLoad - function, run after response
	 * @private
	 */
	_sendLoadScriptRequest(scriptDbSelector, onLoad) {
		GM_xmlhttpRequest({
			method: "POST",
			url: this.serverUrl,
			data: `selector=${encodeURIComponent(scriptDbSelector)}`,
			headers: {"Content-Type": "application/x-www-form-urlencoded"},
			onload: response => {
				let text = response.responseText;

				let shortText = text.substr(0, 50) + "...";
				console.debug(`ScriptLoader: "${scriptDbSelector}" has loaded\n${shortText}`);

				if (onLoad)
					onLoad(JSON.parse(text));
			},
			onerror: () => console.debug(`ScriptLoader: Cant load script (name=${scriptDbSelector})`)
		});
	}
}